package com.ai.imageclassification

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.provider.MediaStore

import android.content.Intent

import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.os.Build

import com.ai.imageclassification.R
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import com.ai.imageclassification.ml.Inception
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.ByteBuffer
import java.nio.ByteOrder


class MainActivity : AppCompatActivity() {

    var result: TextView? = null
    var confidence:TextView? = null
    var imageView: ImageView? = null
    var picture: Button? = null
    var imageSize = 224

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        result = findViewById(R.id.result)
        confidence = findViewById<TextView>(R.id.confidence)
        imageView = findViewById<ImageView>(R.id.imageView)
        picture = findViewById<Button>(R.id.button)

        picture!!.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 1)
            } else {
                //Request camera permission if we don't have it.
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 100)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            var bitmap = data!!.extras!!.get("data") as Bitmap
            var dimension = Math.min(bitmap.width, bitmap.height)
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension)
            imageView!!.setImageBitmap(bitmap)

            bitmap = Bitmap.createScaledBitmap(bitmap, imageSize, imageSize, false)
            classifyImage(bitmap)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun classifyImage(bitmap: Bitmap?) {
        val model = Inception.newInstance(this)

        var bmp = bitmap!!.copy(Bitmap.Config.ARGB_8888,true)
        val image = TensorImage.fromBitmap(bmp)


        val outputs = model.process(image)
        val probability = outputs.probabilityAsCategoryList

        // find the index of the class with the biggest confidence.
        // find the index of the class with the biggest confidence.
        var maxPos = 0
        var maxConfidence = 0f
        var name = ""
        for (value in probability) {
            if (value.score > maxConfidence) {
                maxConfidence = value.score
                name = value.label
            }
        }


        result!!.text = name
//
//        var s = ""
//        for (i in classes.indices) {
//            s += String.format("%s: %.1f%%\n", classes[i], confidences[i] * 100)
//        }
        confidence!!.text = (maxConfidence * 100).toInt().toString() + "%"


        model.close()
    }
}